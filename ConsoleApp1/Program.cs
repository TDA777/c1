﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] ags)
        {

            int min1 = minVal(5, 3);
            int[] args = { 45, 67, 34, 9, 112, 8 };
            int min2 = minVal(args);
            Console.WriteLine(min1 + " " + min2);


            Console.Read();
        }
        

        public static int minVal(params int[] param)
        {
            int m = param[0];
            for (int i = 1; i < param.Length; i++)
                if (param[i] < m) m = param[i];
            return m;
        }

    }
}
